package group.golden;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@Builder
public class FraudCheckHistory {
    @Id
    private String id;
    private String customerId;
    private Boolean isFraudster;
    private LocalDateTime createdAt;
}
