package group.golden;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public record FraudCheckService(
        FraudCheckHistoryRepository checkHistoryRepository
) {
    public boolean isFraudulentCustomer(String customerId) {
        this.checkHistoryRepository.save(
                FraudCheckHistory.builder()
                        .isFraudster(FALSE)

                        .build()
        );
        return TRUE;
    }
}
