package group.golden;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email
) {
}
